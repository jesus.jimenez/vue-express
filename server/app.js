const path = require("path");
const fs = require("fs");

const express = require("express");
const history = require("connect-history-api-fallback");

const historyMiddleware = history({
	disableDotRule: true,
	verbose: true,
});

const app = express();

app.use(express.static(path.join(__dirname, "./public")));

//This will redirect all request except /api endpoint to the SPA
app.use((req, res, next) => {
	if (/\/api\/*/.test(req.path)) {
		next();
	} else {
		//If not /api, then use the history api middleware
		historyMiddleware(req, res, next);
	}
});
//And lastly again in order to serve the static files not matching the SPA
app.use(express.static(path.join(__dirname, "./public")));

app.get("/api", (req, res) => {
	res.send("Holamundo");
});

app.listen(8081, () => {
	console.log("starting app");
});
