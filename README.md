# Vue and Express integration template

With this template, you can serve a vue application from express at the same time as express has all the REST API functionality. This way, only executing one command, you can deploy and run in production the website and API together.